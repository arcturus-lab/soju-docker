FROM golang:1.23 as build

WORKDIR /src

COPY soju/ ./

RUN make soju

FROM gcr.io/distroless/base-debian12

COPY --from=build /src/soju /soju
COPY --from=build /src/sojuctl /sojuctl
COPY --from=build /src/sojudb /sojudb
COPY --from=build /src/LICENSE /LICENSE 

EXPOSE 6667

ENTRYPOINT ["/soju", "--config", "soju.cfg"]
